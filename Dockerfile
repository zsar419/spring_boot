FROM tomcat:8.0-jre8-alpine
MAINTAINER "zeeshan <zsar419@aucklanduni.ac.nz">
VOLUME /tmp
ADD target/*.war /usr/local/tomcat/webapps/infovetint.war